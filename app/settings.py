import os
import logging.config
from pathlib import Path
from decouple import config

BASE_DIR = Path(__file__).resolve().parent.parent


# logging configuration
LOGGING_CONFIGURATION = {
    "version": 1,
    "handlers": {
        "fileHandler": {
            "class": "logging.FileHandler",
            "formatter": "myFormatter",
            "filename": os.path.join(BASE_DIR, "scheduler_logs.log")
        },
        "consoleHandler": {
            'class': 'logging.StreamHandler',
            'formatter': 'myFormatter'
        }
    },
    "loggers": {
        "scheduler": {
            "handlers": ["consoleHandler", "fileHandler"],
            "level": "INFO",
        }
    },
    "formatters": {
        "myFormatter": {
            "format": "%(levelname)s - %(asctime)s - %(filename)s - %(message)s"
        }
    }
}

logging.config.dictConfig(LOGGING_CONFIGURATION)
logger = logging.getLogger('scheduler')


# email configuration
IMAP_HOST = config('IMAP_HOST')
IMAP_PORT = config('IMAP_PORT')
IMAP_USER = config('IMAP_USER')
IMAP_PASSWORD = config('IMAP_PASSWORD')
IMAP_MAILBOX = config('IMAP_MAILBOX')

SMTP_HOST = config('SMTP_HOST')
SMTP_PORT = config('SMTP_PORT')
SMTP_USER = config('SMTP_USER')
SMTP_PASSWORD = config('SMTP_PASSWORD')


# database configuration (support: mssql, postgresql, oracle, mysql)
DATABASE_NAME = config('DATABASE_NAME')
DATABASE_HOST = config('DATABASE_HOST')
DATABASE_PORT = config('DATABASE_PORT')
DATABASE_USER = config('DATABASE_USER')
DATABASE_PASSWORD = config('DATABASE_PASSWORD')

DATABASE_CONNECT = f'mssql+pyodbc://{DATABASE_USER}:{DATABASE_PASSWORD}@'\
                   f'{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}?driver=SQL+Server'


# celery configuration
BROKER_USER = config('BROKER_USER')
BROKER_PASSWORD = config('BROKER_PASSWORD')
BROKER_HOST = config('BROKER_HOST')
BROKER_PORT = config('BROKER_PORT')

BROKER_URL = f'amqp://{BROKER_USER}:{BROKER_PASSWORD}@{BROKER_HOST}:{BROKER_PORT}//'
RESULT_BACKEND_CONNECT = f'db+mssql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/'\
                         f'{DATABASE_NAME}?driver=SQL+Server'

CELERY_CONF = dict(
    broker_url=BROKER_URL,
    timezone='Europe/Moscow',
    result_backend=RESULT_BACKEND_CONNECT,
    database_table_names={
        'task': 'scheduler_task_meta',
        'group': 'scheduler_group_meta'
    },
    result_extended=True,
    imports=('app.tasks',)
)
