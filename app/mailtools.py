import os
import imaplib
import smtplib
from datetime import datetime
from email import encoders
from email.header import decode_header
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.parser import BytesParser
from pprint import pprint
from uuid import uuid4
from dateutil.parser import parse
from jinja2 import Template

from app.database_methods import create_log
from app.data_validation import MerchScheduleInfoObject
from app.settings import (IMAP_PORT, IMAP_HOST, IMAP_USER, IMAP_PASSWORD, IMAP_MAILBOX, logger, BASE_DIR,
                          SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD)


class BaseMailToolObject(object):
    """
    Общий суперкласс. Используется для включения и отключения логики,
    общей для всех наследующих классов
    """
    pass


class MailFetchObject(BaseMailToolObject):

    def __init__(self):
        self.host = IMAP_HOST
        self.port = IMAP_PORT
        self.user = IMAP_USER
        self.password = IMAP_PASSWORD
        self.attachments_dir = os.path.join(BASE_DIR, 'attachments')
        self.connection = self._open_connection()
        self.connection.select(IMAP_MAILBOX)

    def _open_connection(self):
        try:
            logger.info(f'Connecting to {self.host}')
            connect = imaplib.IMAP4_SSL(host=self.host, port=self.port)
            logger.info(f'Logging as {self.user}')
            connect.login(user=self.user, password=self.password)
            return connect
        except Exception as err:
            logger.error(err, exc_info=True)

    def _create_work_directory(self):
        wd = os.path.join(self.attachments_dir, str(uuid4()))
        if not os.path.isdir(wd):
            os.makedirs(wd)
        return wd

    def check_mailbox(self):
        typ, msg_ids = self.connection.search(None, '(UNSEEN)')
        if len(msg_ids) == 1 and not msg_ids[0]:
            logger.info(f'No unseen messages')
            return None
        msg_ids = msg_ids[0].split()
        logger.info(f'Unseen messages: {len(msg_ids)}')
        return msg_ids

    @staticmethod
    def _decode_field(field):
        data = decode_header(field)
        decoded_field = []
        for part in data:
            if isinstance(part[0], bytes):
                if part[1] is not None:
                    decoded_field.append(part[0].decode(part[1]))
                if part[1] is None:
                    decoded_field.append(part[0].decode('raw-unicode-escape'))
            if isinstance(part[0], str):
                decoded_field.append(part[0])
        return ' '.join(decoded_field)

    @staticmethod
    def check_filename(file_name):
        return len(os.path.split(file_name)[1].split('.')) == 2

    def extract_letter(self, message_num):
        message_dir = self._create_work_directory()
        fetch_status, data = self.connection.fetch(message_num, '(RFC822)')
        if fetch_status:
            raw_message = data[0][1]
            parser = BytesParser()
            message_obj = parser.parsebytes(raw_message)
            all_data = {k.lower(): v for k, v in message_obj.items()}
            required_data = {
                'message_id': all_data['message-id'].strip(),  # log
                'message_date': parse(all_data['date']),  # log, answer
                'sender': all_data['return-path'],  # log, answer
                'recipient': self._decode_field(all_data['to']),  # answer
                'subject': self._decode_field(all_data.get('subject', '<без темы>')),  # log, answer
                'email_provider': all_data['received-spf'].split()[4],  # log
                'message_dir': message_dir,  # processing
                'state': 'correct'  # marker
            }
            for part in message_obj.walk():
                """предусмотреть отсутствие вложения"""
                if 'application' in part.get_content_type():
                    file_name, encoding = decode_header(part.get_filename())[0]
                    file_name = file_name.decode(encoding) if encoding is not None else file_name
                    if not self.check_filename(file_name):
                        required_data['state'] = 'incorrect_filename'
                    if self.check_filename(file_name):
                        file_path = os.path.join(message_dir, file_name)
                        required_data['filename'] = file_name  # processing
                        if not os.path.isfile(file_path):
                            with open(file_path, 'wb') as f:
                                f.write(part.get_payload(decode=True))
                if part.get_content_type() == 'text/html':
                    required_data['original_message_body'] = part.get_payload(decode=True).decode()  # answer
            if not required_data.get('filename') and required_data.get('state') == 'correct':
                required_data['state'] = 'not_attachment'
            return required_data


class MailSendObject(BaseMailToolObject):

    def __init__(self, **kwargs):
        self.host = SMTP_HOST
        self.port = SMTP_PORT
        self.user = SMTP_USER
        self.password = SMTP_PASSWORD
        for k, v in kwargs.items():
            setattr(self, k, v)

    def _create_context(self):
        return dict(
            sender=getattr(self, 'sender', None),
            subject=getattr(self, 'subject', None),
            message_date=getattr(self, 'message_date', None),
            recipient=getattr(self, 'recipient', None),
            original_message_body=getattr(self, 'original_message_body', None),
            errors=getattr(self, 'errors', None)
        )

    def _open_connection(self):
        try:
            connect = smtplib.SMTP(host=self.host, port=self.port)
            connect.starttls()
            connect.login(user=self.user, password=self.password)
            return connect
        except Exception as err:
            logger.error(err, exc_info=True)

    @staticmethod
    def _add_attachment(main_msg, attachment_dir):
        data = open(attachment_dir, mode='rb')
        msg = MIMEBase('application', 'zip')
        msg.set_payload(data.read())
        data.close()
        encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', f'attachment; filename={os.path.basename(attachment_dir)}')
        main_msg.attach(msg)
        return main_msg

    @staticmethod
    def _get_template(template_name, extension):
        return os.path.join(BASE_DIR, 'auto_answers', f'{template_name}.{extension}')

    def render_text_plain(self):
        template_name = getattr(self, 'state')
        template_path = self._get_template(template_name, 'txt')
        context = self._create_context()
        with open(file=template_path, mode='r', encoding='utf-8') as f:
            template_string = f.read()
        template_string = Template(template_string)
        if context:
            rendered_answer = template_string.render(context)
            return rendered_answer
        if not context:
            rendered_answer = template_string.render()
            return rendered_answer

    def render_text_html(self):
        template_name = getattr(self, 'state')
        template_path = self._get_template(template_name, 'html')
        context = self._create_context()
        with open(file=template_path, mode='r', encoding='utf-8') as f:
            template_string = f.read()
        template_string = Template(template_string)
        if context:
            rendered_answer = template_string.render(context)
            return rendered_answer
        if not context:
            rendered_answer = template_string.render()
            return rendered_answer

    def send_answer(self):
        attachment_dir = getattr(self, 'badges_dir', None)

        main_msg = MIMEMultipart('mixed')

        main_msg.add_header('To', getattr(self, 'sender'))
        main_msg.add_header('From', SMTP_USER)
        main_msg.add_header('Subject', 'Re: ' + getattr(self, 'subject'))
        main_msg.add_header('In-Reply-To', getattr(self, 'message_id'))
        main_msg.add_header('References', getattr(self, 'message_id'))

        body_text_plain = self.render_text_plain()
        body_text_html = self.render_text_html()
        body = MIMEMultipart('alternative')
        body.attach(MIMEText(body_text_plain, 'plain'))
        body.attach(MIMEText(body_text_html, 'html'))

        main_msg.attach(body)

        if attachment_dir:
            self._add_attachment(main_msg, attachment_dir)

        connect = self._open_connection()
        connect.send_message(msg=main_msg, from_addr=SMTP_USER, to_addrs=[main_msg["To"]])
        connect.quit()
