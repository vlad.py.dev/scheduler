from app.celery import app
from app.mailtools import MailSendObject


@app.task
def send_answer(answer_data):
    answer_obj = MailSendObject(**answer_data)
    answer_obj.send_answer()

