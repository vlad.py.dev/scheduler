import os
from datetime import datetime
from openpyxl import load_workbook

from app.creating_barcodes import MerchBarcodeObject
from app.database_methods import check_shop, check_provider, check_weekday, DatabaseAccessObject
from app.settings import logger

INCORRECT_FILENAME = 'incorrect_filename'
HIDDEN_SHEET_DISPLAYED = 'hidden_sheet_displayed'
TEMPLATE_FILLING_ERROR = 'template_filling_error'
NOT_ATTACHMENT = 'not_attachment'
CORRECT = 'correct'


class ExcelDataValidatorObject(object):
    """
    класс для валидации данных согласно заданной логике
    """
    excel_db_fields_mapping = {
        'Наименование ММ': 'shop_name',
        'Код ММ': 'shop_code',
        'Адрес ММ': 'shop_address',
        'Наименование поставщика': 'provider_name',
        'Обслуживаемый филиал': 'shop_branch',
        'ФИО МЧ': 'fio',
        'День недели посещения': 'weekday'
    }

    def __init__(self, file_path, sender, message_date):
        self.workbook = load_workbook(file_path, data_only=True)
        self.sender = sender
        self.message_date = message_date
        self.errors = []
        self.sheet_names = self.workbook.sheetnames
        self.work_sheet = self.workbook['Шаблон']
        self.mode = self.get_mode()
        self.column_names = self.get_column_names()
        self.db_column_names = self.get_db_column_names()
        self.work_space = self.get_work_space()

    def get_column_names(self):
        """Получить имена полей"""
        return {cell.column: cell.value for cell in self.work_sheet['A3':'G3'][0]}

    def get_db_column_names(self):
        """Сопоставить имена полей в БД с именами полей в шаблоне"""
        return [self.excel_db_fields_mapping[cell.value] for cell in self.work_sheet['A3':'G3'][0]]

    def check_sheets(self):
        """Проверка на скрытый лист"""
        hidden_sheet = self.workbook['Выпадающие списки']
        return hidden_sheet.sheet_state == 'hidden'

    def get_mode(self):
        """Тип шаблона"""
        return self.work_sheet['B1'].value

    def get_work_space(self):
        """Получить диапазон заполненных ячеек относительно всей таблицы"""
        return [r[:7] for r in self.work_sheet.rows if r[0].value is not None][2:]

    @staticmethod
    def check_empty(cell):
        return cell.value is not None and cell.value != ' '

    @staticmethod
    def check_fio(fio):
        """Проверить корректность ФИО"""
        try:
            fio = fio.split()
        except AttributeError:
            return False
        len_check = len(fio) >= 2
        len_each_part_check = all(len(i) > 1 for i in fio)
        upper_case_check = all(not i[:2].isupper() for i in fio)
        return len_check and len_each_part_check and upper_case_check

    def row_to_dict(self, row):
        row_schedule = dict(zip(self.db_column_names, [cell.value for cell in row]))
        row_schedule.update({'sender': self.sender, 'message_date': self.message_date})
        return row_schedule

    def clean(self):
        """Основной цикл проверки данных"""
        buffer = []
        for row in self.work_space:
            row_errors = []
            for cell in row:
                if not self.check_empty(cell):
                    row_errors.append(f'Не заполнена ячейка {cell.coordinate}. Строка {cell.row}')
                if cell.column == 1 and not check_shop(cell.value):
                    row_errors.append(f'Некорректно заполнено поле {self.column_names[cell.column]}. Строка {cell.row}')
                if cell.column == 4 and not check_provider(cell.value):
                    row_errors.append(f'Некорректно заполнено поле {self.column_names[cell.column]}. Строка {cell.row}')
                if cell.column == 6 and not self.check_fio(cell.value):
                    row_errors.append(f'Некорректно заполнено поле {self.column_names[cell.column]}. Строка {cell.row}')
                if cell.column == 7 and not check_weekday(cell.value):
                    row_errors.append(f'Некорректно заполнено поле {self.column_names[cell.column]}. Строка {cell.row}')
            if not row_errors:
                buffer.append(self.row_to_dict(row))
            self.errors[:0] = row_errors
        self.errors = list(set(self.errors))
        return buffer

    @staticmethod
    def sort_buffer(buffer=None):
        """Сортировка валидных данных и приведение к виду:
        {(merch_fio, provider_name): [{словарь со строкой из файла}]}"""
        sorted_data = {}
        # получаем уникальные пары: ФИО мерча + поставщик
        merch_provider_pairs = set([(i['fio'], i['provider_name']) for i in buffer])
        # сортируем данные
        for pair in merch_provider_pairs:
            pair_data = []
            for row in buffer:
                row = row.copy()
                if row['fio'] == pair[0] and row['provider_name'] == pair[1]:
                    row.pop('fio')
                    row.pop('provider_name')
                    pair_data.append(row)
            sorted_data[pair] = pair_data
        return sorted_data


class MerchScheduleInfoObject(object):
    """
    Класс создается в цикле прохода по непрочитанным письмам
    содержит в себе данные по самому письму, вложению, корректной информации и ошибкам
    """

    def __init__(self, message_id, message_date, email_provider, sender, subject, file_name, message_dir, state):
        self.message_id = message_id
        self.message_date = message_date
        self.email_provider = email_provider
        self.sender = sender
        self.subject = subject
        self.file_name = file_name
        self.message_dir = message_dir
        self.state = state
        self.errors = []
        self.validator = ExcelDataValidatorObject(
            file_path=os.path.join(self.message_dir, self.file_name),
            sender=self.sender,
            message_date=self.message_date
        )

    def run_processing(self):
        totals = dict(subject=self.subject, sender=self.sender, state=self.state, message_date=self.message_date)
        if self.validator.check_sheets():
            buffer = self.validator.clean()
            self.errors = self.validator.errors  # ошибки из объекта ExcelDataValidatorObject
            sorted_data = self.validator.sort_buffer(buffer)
            db = DatabaseAccessObject(sorted_data)
            if self.validator.mode == 'Оформление допуска':
                db.open_access()
                data_for_barcodes = db.new_merchandisers
                barcodes = MerchBarcodeObject(merchandisers_data=data_for_barcodes, work_dir=self.message_dir)
                barcodes.create_badges()
                totals.update({'badges_dir': barcodes.make_zip(), 'processing_date': datetime.now()})
            if self.validator.mode == 'Изменение графика':
                db.change_schedule()
                totals.update({'processing_date': datetime.now()})
            if self.validator.mode == 'Увольнение':
                db.dismissal()
                totals.update({'processing_date': datetime.now()})
            db.session.commit()
            self.errors[:0] = db.extract_errors()  # ошибки из объекта DatabaseAccessObject
            if self.errors:
                logger.error(f'{self.message_id} - {TEMPLATE_FILLING_ERROR}')
                totals['state'] = TEMPLATE_FILLING_ERROR
                totals.update({'errors': self.errors})
            else:
                logger.info(f'{self.message_id} - {CORRECT}')
        else:
            logger.error(f'{self.message_id} - {HIDDEN_SHEET_DISPLAYED}')
            totals.update({'state': HIDDEN_SHEET_DISPLAYED, 'processing_date': datetime.now()})
        return totals
