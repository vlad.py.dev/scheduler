from datetime import datetime

from app import Session
from app.database_models import *


def get_providers() -> list:
    session = Session()
    providers = session.query(Provider).all()
    session.close()
    return [i.provider_name for i in providers]


def get_mms() -> list:
    session = Session()
    mm_names = session.query(Shop).all()
    session.close()
    return [i.name for i in mm_names]


def get_weekdays() -> list:
    session = Session()
    weekdays = session.query(Weekday).all()
    session.close()
    return [i.weekday for i in weekdays]


def create_log(sender, subject, message_id, message_date, processing_date, result, answer, email_provider):
    session = Session()
    log = Log(message_id=message_id, subject=subject, sender=sender, message_date=message_date,
              processing_date=processing_date, result=result, answer=answer, email_provider=email_provider)
    session.add(log)
    session.commit()
    log_id = log.id
    session.close()
    return log_id


PROVIDERS = get_providers()
MMS = get_mms()
WEEKDAYS = get_weekdays()


def check_provider(provider_name):
    return provider_name in PROVIDERS


def check_shop(mm_name):
    return mm_name in MMS


def check_weekday(weekday):
    return weekday in WEEKDAYS


class DatabaseAccessObject(object):

    def __init__(self, sorted_data=None):
        self.sorted_data = sorted_data
        self.session = Session()
        self.errors = []
        self.created_objects = []
        self.new_merchandisers = []

    def get_provider(self, provider_name):
        """Получить объект поставщика"""
        provider = self.session.query(Provider).filter_by(provider_name=provider_name).all()[0]
        return provider

    def get_merchandiser(self, provider_name, fio):
        """Получить объект мерча"""
        provider = self.get_provider(provider_name)
        merchandiser = self.session.query(Merchandiser).filter_by(fio=fio, provider=provider).all()[0]
        return merchandiser

    def get_merch_schedules(self, merch):
        """Получить все графики мерча"""
        schedules = self.session.query(Schedule).filter_by(merchandiser=merch).all()
        return schedules

    def merchandiser_exist(self, provider_name, fio):
        """Проверка существования конкретного мерча у конкретного поставщика"""
        provider = self.session.query(Provider).filter_by(provider_name=provider_name).all()[0]
        merchandisers = [i.fio for i in provider.merchandisers]
        return fio in merchandisers

    def create_merchandiser(self, provider_name, fio):
        """Создать объект мерчендайзера"""
        provider = self.get_provider(provider_name)
        new_merchandiser = Merchandiser(fio=fio)
        new_merchandiser.provider = provider
        return new_merchandiser

    @staticmethod
    def create_schedule(row):
        """Создать объект графика"""
        load_date = datetime.now()
        row.update({'load_date': load_date})
        schedule = Schedule(**row)
        return schedule

    def delete_schedules(self, schedules):
        """Удалить графики"""
        for s in schedules:
            self.session.delete(s)
        return None

    def open_access(self):
        """Логика для вида шаблона [открытие допуска]"""
        for k, v in self.sorted_data.items():
            row_merch = {'provider_name': k[1], 'fio': k[0]}
            if not self.merchandiser_exist(**row_merch):
                new_merchandiser = self.create_merchandiser(**row_merch)
                self.session.commit()
                new_merchandiser.create_barcode()
                self.created_objects.append(new_merchandiser)
                self.new_merchandisers.append({'fio': new_merchandiser.fio, 'merch_id': new_merchandiser.barcode})
                for row in v:
                    new_schedule = self.create_schedule(row=row)
                    new_schedule.merchandiser = new_merchandiser
                    self.created_objects.append(new_schedule)
            else:
                self.errors.append(
                    f'Мерчендайзер {row_merch["fio"]} уже зарегистрирован от поставщика {row_merch["provider_name"]}')
        self.session.add_all(self.created_objects)
        self.session.commit()

    def change_schedule(self):
        """Логика для вида шаблона [изменение графика]"""
        for k, v in self.sorted_data.items():
            row_merch = {'provider_name': k[1], 'fio': k[0]}
            if self.merchandiser_exist(**row_merch):
                merchandiser = self.get_merchandiser(**row_merch)
                existing_schedules = self.get_merch_schedules(merchandiser)
                self.delete_schedules(existing_schedules)
                for row in v:
                    new_schedule = self.create_schedule(row=row)
                    new_schedule.merchandiser = merchandiser
                    self.created_objects.append(new_schedule)
                self.session.add_all(self.created_objects)
            else:
                self.errors.append(
                    f'Мерчендайзер {row_merch["fio"]} не зарегистрирован от поставщика {row_merch["provider_name"]}')

    def dismissal(self):
        """Логика для вида шаблона [увольнение]"""
        for k, v in self.sorted_data.items():
            row_merch = {'provider_name': k[1], 'fio': k[0]}
            provider = self.get_provider(row_merch['provider_name'])
            if self.merchandiser_exist(**row_merch):
                merchandiser = self.get_merchandiser(fio=row_merch['fio'], provider_name=provider.provider_name)
                for s in merchandiser.schedules:
                    self.session.delete(s)
                self.session.delete(merchandiser)
            else:
                self.errors.append(
                    f'Мерчендайзер {row_merch["fio"]} не зарегистрирован от поставщика {row_merch["provider_name"]}')

    def extract_errors(self):
        return list(set(self.errors))
