from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.settings import DATABASE_CONNECT
from app.celery import app

__all__ = ('app', )


engine = create_engine(DATABASE_CONNECT, echo=True)
Session = sessionmaker(bind=engine)
