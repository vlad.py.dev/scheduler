import base64
import os
import shutil


def encode_base64(arg):
    """кодирование строки в base64"""
    str_bytes = arg.encode('utf-8')
    b64_bytes = base64.b64encode(str_bytes)
    return b64_bytes.decode('utf-8')


def decode_base64(arg):
    """декодирование строки из base64"""
    b64_bytes = arg.encode('utf-8')
    str_bytes = base64.b64decode(b64_bytes)
    return str_bytes.decode('utf-8')


def clear_directory(directory):
    os.chdir(directory)
    dirs = os.listdir(directory)
    for d in dirs:
        shutil.rmtree(d)