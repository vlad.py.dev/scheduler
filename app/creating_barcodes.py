from barcode.codex import Code39
from barcode.writer import ImageWriter
from jinja2 import Template
import weasyprint
import os
import shutil
import zipfile
from app.settings import BASE_DIR

BARCODE_TMP = os.path.join(BASE_DIR, 'barcode_tmp')


class MerchBarcodeObject(object):
    TEMP_DIR = BARCODE_TMP

    def __init__(self, merchandisers_data, work_dir):
        self.merchandisers_data = merchandisers_data
        self.work_dir = work_dir
        os.chdir(self.work_dir)

    @staticmethod
    def _create_barcode_png(merch_id):
        b = Code39(merch_id, writer=ImageWriter(), add_checksum=False)
        options = dict(module_height=9.0, dpi=250, text_distance=3.5)
        filename = b.save(filename='temp', options=options)
        return filename

    def _render_html_template(self, fio):
        with open(os.path.join(self.TEMP_DIR, 'template.html'), encoding='utf-8') as f:
            string = f.read()

        tm = Template(string)
        html_badge = tm.render(fio=fio)

        with open(os.path.join(self.work_dir, 'output.html'), encoding='utf-8', mode='w') as f:
            f.write(html_badge)

    def _create_pdf(self, fio):
        weasyprint.HTML(filename=os.path.join(self.work_dir, 'output.html')).write_pdf(
            os.path.join(self.work_dir, f'{fio}.pdf'),
            stylesheets=[weasyprint.CSS(
                os.path.join(self.TEMP_DIR, 'styles.css'))])

    def create_badges(self):
        for m in self.merchandisers_data:
            self._create_barcode_png(m['merch_id'])
            self._render_html_template(m['fio'])
            self._create_pdf(m['fio'])

    def make_zip(self):
        files = os.listdir(self.work_dir)
        files = [f for f in files if f.split('.')[1] == 'pdf']
        if files:
            with zipfile.ZipFile('barcodes.zip', mode='w') as zf:
                for file in files:
                    zf.write(file)
            return os.path.join(self.work_dir, 'barcodes.zip')
