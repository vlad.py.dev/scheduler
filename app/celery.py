from celery import Celery
# from app.settings import (DATABASE_ENGINE, DATABASE_USER, DATABASE_PASSWORD, DATABASE_HOST, DATABASE_PORT,
#                           DATABASE_NAME, BROKER_USER, BROKER_PASSWORD, BROKER_HOST, BROKER_PORT)
from app.settings import CELERY_CONF

app = Celery('app')
app.conf.update(CELERY_CONF)

# run celery celery -A app worker -l info --pool=solo

# app.conf.update(
#     broker_url=f'amqp://{BROKER_USER}:{BROKER_PASSWORD}@{BROKER_HOST}:{BROKER_PORT}//',
#     timezone='Europe/Moscow',
#     result_backend=f'db+{DATABASE_ENGINE}://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/'
#                    f'{DATABASE_NAME}?driver=SQL+SERVER',
#     database_table_names={
#         'task': 'scheduler_task_meta',
#         'group': 'scheduler_group_meta'
#     },
#     result_extended=True,
#     imports=('app.tasks', )
# )
