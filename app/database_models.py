from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from app import engine


Base = declarative_base()


class Provider(Base):
    __tablename__ = 'T_PROVIDERS_MM'
    id = Column(Integer, primary_key=True)
    provider_name = Column(String(255), unique=True)

    merchandisers = relationship('Merchandiser', back_populates='provider')

    def __repr__(self):
        return self.provider_name


class Merchandiser(Base):
    __tablename__ = 'T_MERCHANDISERS_MM'
    id = Column(Integer, primary_key=True)
    fio = Column(String(255), nullable=False)
    barcode = Column(String(155))
    provider_id = Column(Integer, ForeignKey('T_PROVIDERS_MM.id'))

    provider = relationship('Provider', back_populates='merchandisers')
    schedules = relationship('Schedule', back_populates='merchandiser', cascade='all, delete')

    def __repr__(self):
        return self.fio

    def create_barcode(self):
        if self.id:
            self.barcode = 'MERCH' + str(self.id)


class Shop(Base):
    __tablename__ = 'T_OPEN_SHOPS_MM'
    id = Column(Integer, primary_key=True)
    name = Column(String(155))
    code = Column(String(6))
    address = Column(String(500))
    branch = Column(String(155))

    def __repr__(self):
        return self.name


class Weekday(Base):
    __tablename__ = 'T_WEEKDAYS'
    id = Column(Integer, primary_key=True)
    weekday = Column(String(15))

    def __repr__(self):
        return self.weekday


class Schedule(Base):
    __tablename__ = 'T_SCHEDULES_MM'
    id = Column(Integer, primary_key=True)
    shop_name = Column(String(255), nullable=False)
    shop_code = Column(String(6), nullable=False, index=True)
    shop_address = Column(String(512), nullable=False)
    shop_branch = Column(String(155), nullable=False)
    weekday = Column(String(50), nullable=False)
    sender = Column(String(256), nullable=False)
    message_date = Column(DateTime, nullable=False)
    load_date = Column(DateTime, nullable=False)
    merchandiser_id = Column(Integer, ForeignKey('T_MERCHANDISERS_MM.id'))

    merchandiser = relationship('Merchandiser', back_populates='schedules')

    def __repr__(self):
        return self.shop_name


class Log(Base):
    __tablename__ = 'T_SCHEDULER_LOG'
    id = Column(Integer, primary_key=True)
    message_id = Column(String(500))
    email_provider = Column(String(500))
    subject = Column(String(1024))
    sender = Column(String(255))
    message_date = Column(DateTime)
    processing_date = Column(DateTime)
    result = Column(String(50))
    answer = Column(Text)

    def __repr__(self):
        return f'Sender: {self.sender}, subject: {self.subject}'


Base.metadata.create_all(engine)  # каждый раз проверять, синхронизированы ли модели с БД. Если нет - таблицы создадутся
