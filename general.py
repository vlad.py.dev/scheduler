import os
from datetime import datetime

from app.data_validation import MerchScheduleInfoObject, NOT_ATTACHMENT, INCORRECT_FILENAME
from app.utils import clear_directory
from app.mailtools import MailFetchObject, MailSendObject
from app.database_methods import create_log
from app.tasks import send_answer
from app.settings import BASE_DIR
from app.settings import logger


if not os.path.isdir(os.path.join(BASE_DIR, 'attachments')):
    os.makedirs(os.path.join(BASE_DIR, 'attachments'))

logger.info(f'{"*" * 15} START {"*" * 15}')

connection = MailFetchObject()
unseen_messages = connection.check_mailbox()
if unseen_messages:
    for message_id in unseen_messages:
        message_data = connection.extract_letter(message_id)
        if message_data.get('state') == 'not_attachment':
            # автоответ нет вложения
            answer_obj = MailSendObject(**message_data)
            # answer_obj.send_answer()
            send_answer.delay(message_data)
            logger.error(f'{message_data["message_id"]} - {NOT_ATTACHMENT}')
            create_log(message_id=message_data['message_id'], subject=message_data['subject'],
                       sender=message_data['sender'], message_date=message_data['message_date'],
                       processing_date=datetime.now(), result=message_data['state'],
                       answer=answer_obj.render_text_html(), email_provider=message_data['email_provider'])
            continue
        if message_data.get('state') == 'incorrect_filename':
            # автоответ кривое название файла
            answer_obj = MailSendObject(**message_data)
            # answer_obj.send_answer()
            send_answer.delay(message_data)

            logger.error(f'{message_data["message_id"]} - {INCORRECT_FILENAME}')
            create_log(message_id=message_data['message_id'], subject=message_data['subject'],
                       sender=message_data['sender'], message_date=message_data['message_date'],
                       processing_date=datetime.now(), result=message_data['state'],
                       answer=answer_obj.render_text_html(), email_provider=message_data['email_provider'])
            continue
        # если есть вложение и его наименование не кривое, то запускаем обработку
        processing = MerchScheduleInfoObject(
            message_id=message_data['message_id'],
            message_date=message_data['message_date'],
            email_provider=message_data['email_provider'],
            sender=message_data['sender'],
            subject=message_data['subject'],
            file_name=message_data['filename'],
            message_dir=message_data['message_dir'],
            state=message_data['state']
        )
        totals = processing.run_processing()
        totals.update({
            'recipient': message_data['recipient'], 'original_message_body': message_data['original_message_body'],
            'message_id': message_data['message_id']
        })
        # автоответ
        answer_obj = MailSendObject(**totals)
        # answer_obj.send_answer()  # раскомментить, чтобы отправлять синхронно

        send_answer.delay(totals)  # закомментить, чтобы выполнять синхронно

        create_log(
            sender=totals['sender'], subject=totals['subject'], message_id=message_data['message_id'],
            message_date=totals['message_date'], processing_date=totals['processing_date'],
            result=totals['state'], answer=answer_obj.render_text_html(), email_provider=message_data['email_provider']
        )


connection.connection.logout()

clear_directory(os.path.join(BASE_DIR, 'attachments'))

logger.info(f'{"*" * 15} FINISH {"*" * 15}')
